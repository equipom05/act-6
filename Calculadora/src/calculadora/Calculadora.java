/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.util.Scanner;

/**
 *
 * @author Danib
 */
public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String operacion = sc.next();
        
        System.out.println(calcular(operacion));
    }

    public static int calcular(String op){
        int resultado = 0;
        
        String[] operacion = op.split("([0-9])");
        String[] numeros = op.split("([\\+|\\*|\\/|\\-])");
        
        int num1 = Integer.parseInt(numeros[0]);
        int num2 = Integer.parseInt(numeros[1]);
        
        if(operacion[1].equals("+")){
            resultado = num1 + num2;
        } else if(operacion[1].equals("-")){
            resultado = num1 - num2;
        } else if(operacion[1].equals("*")){
            resultado = num1 * num2;
        } else {
            resultado = num1 / num2;
        }
        
        return resultado;
    }
    
}
