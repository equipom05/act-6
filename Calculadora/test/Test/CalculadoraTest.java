/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import calculadora.Calculadora;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author alumne
 */
public class CalculadoraTest {
    
    public CalculadoraTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of main method, of class Cajanegra.
     */
    /*@Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        //Cajanegra.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    @Test
    public void testSuma_caso1(){        
        String suma = "7+2";
        Calculadora instance = new Calculadora();
        int expResult = 9;
        int result = instance.calcular(suma);
        assertEquals(expResult, result);        
    }

    @Test
    public void testResta_caso2(){        
        String resta = "5-1";
        Calculadora instance = new Calculadora();
        int expResult = 4;
        int result = instance.calcular(resta);
        assertEquals(expResult, result);        
    }
    
    @Test
    public void testMulti_caso3(){        
        String multi = "7*3";
        Calculadora instance = new Calculadora();
        int expResult = 21;
        int result = instance.calcular(multi);
        assertEquals(expResult, result);        
    }
    
    @Test
    public void testDivision_caso4() {        
        String division = "10/5";
        Calculadora instance = new Calculadora();
        int expResult = 2;
        int result = instance.calcular(division);
        assertEquals(expResult, result);        
    }
    
}
